<?php

declare(strict_types=1);

namespace StrategyManager\Engine;

interface StrategyEngineInterface
{
    /**
     * @param array $config
     * @return void
     */
    public function setConfig(array $config): void;

    /**
     * @param string $groupName
     * @param string $strategyName
     * @param array $parameters
     * @return mixed
     */
    public function getStrategyInstance(string $groupName, string $strategyName, array $parameters = []);
}