<?php
declare(strict_types=1);

namespace StrategyManager\Examples;

class TextStrategy implements StrategyInterface
{
    protected string $engine;

    public function __construct(string $engine)
    {
        $this->engine = $engine;
    }

    public function operate(array $params): void
    {

    }
}