<?php

declare(strict_types=1);

namespace StrategyManager\Tests\Engine;

use Contract\Exceptions\LogicException;
use Contract\Exceptions\ValidationException;
use StrategyManager\Engine\StrategyEngine;
use StrategyManager\Examples\HtmlStrategy;
use StrategyManager\Examples\StrategyInterface;
use StrategyManager\Examples\TextStrategy;
use StrategyManager\Tests\TestCase;

class StrategyEngineTest extends TestCase
{
    protected StrategyEngine $strategyEngine;

    public function setUp()
    {
        $this->strategyEngine = $this->getMockBuilder(StrategyEngine::class)
            ->setMethods()
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function dpSetConfig(): array
    {
        return [
            [
                'demo' => [
                    [
                        'interface' => StrategyInterface::class,
                    ]
                ]
            ],
            [
                'demo' => [
                    [
                        'interface' => StrategyInterface::class,
                        'map' => 'ddd'
                    ]
                ]
            ],
            [
                'demo' => [
                    [
                        'interface' => StrategyInterface::class,
                        'map' => [
                            'text' => []
                        ]
                    ]
                ]
            ],
        ];
    }

    /**
     * @dataProvider dpSetConfig
     * @param array $config
     * @return void
     * @throws ValidationException
     */
    public function testSetConfig(array $config): void
    {
        $this->expectException(ValidationException::class);
        $this->strategyEngine->setConfig($config);
    }

    public function dpGetStrategyInstance(): array
    {
        return [
            [
                [
                    'demo' => [
                        'interface' => StrategyInterface::class,
                        'map' => [
                            'text' => TextStrategy::class,
                            'html' => HtmlStrategy::class,
                        ]
                    ]
                ],
                'demo',
                'html'
            ]
        ];
    }

    /**
     * @dataProvider dpGetStrategyInstance
     * @param array $config
     * @param string $groupName
     * @param string $strategyName
     * @return void
     * @throws ValidationException
     */
    public function testGetStrategyInstance(array $config, string $groupName, string $strategyName): void
    {
        $this->expectException(LogicException::class);
        $this->strategyEngine->setConfig($config);
        $this->strategyEngine->getStrategyInstance($groupName, $strategyName, ['engine' => 'newPaper']);
    }

    public function dpGetStrategyInstance1(): array
    {
        return [
            [
                [
                    'demo' => [
                        'interface' => StrategyInterface::class,
                        'map' => [
                            'text' => TextStrategy::class,
                            'html' => HtmlStrategy::class,
                        ]
                    ]
                ],
                'demo',
                'text'
            ]
        ];
    }

    /**
     * @dataProvider dpGetStrategyInstance1
     * @param array $config
     * @param string $groupName
     * @param string $strategyName
     * @return void
     * @throws ValidationException|LogicException
     */
    public function testGetStrategyInstance1(array $config, string $groupName, string $strategyName): void
    {
        $this->strategyEngine->setConfig($config);
        $strategyInstance = $this->strategyEngine->getStrategyInstance($groupName, $strategyName, ['engine' => 'newPaper']);
        $this->assertTrue($strategyInstance instanceof StrategyInterface);
    }
}